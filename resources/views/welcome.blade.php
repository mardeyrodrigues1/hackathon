@extends('adminlte::page')

@section('title', 'Dashboard')


@section('content')

<div class="form-group row">
    <div class="col-lg-6">
        <div class="card">
            <div id="windy"></div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">

            <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Camera 01 </h3>
                </div>
            </div>


            <form method="POST" action="">
                <div class="row">
                    <div class="col-md-6">
                        <div id="my_camera"></div>
                        <div id="resultado"> </div>
                        <input type="hidden" name="image" class="image-tag">
                    </div>

                    <div class="col-md-2">
                        <div id="results"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@if(count($iot) > 0 )
<!-- /.col-md-6 -->
<div class="col-lg-6">
    <div class="card">
        <div class="card-header border-0">
            <div class="d-flex justify-content-between">
                <h3 class="card-title">Precipitação</h3>
            </div>
        </div>
        <div class="card-body">

            <!-- /.d-flex -->

            <div class="position-relative mb-4">
                <canvas id="chLine" height="180"></canvas>
            </div>


        </div>
    </div>
</div>

@endif



@stop

@section('css')
<style>
    #windy {
        width: 100%;
        height: 550px;
    }
</style>
@stop

@section('js')
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"></script>
<script src="https://api4.windy.com/assets/libBoot.js"></script>
<script src="script.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script>
    const options = {
        // Required: API key
        key: 'lRNT94jmi8E0zTdy6ltWJSxBqCsPLjJ9', // REPLACE WITH YOUR KEY !!!
        // Put additional console output
        verbose: true,
        // Optional: Initial state of the map
        lat: -18.918366,
        lon: -48.266373,
        zoom: 7,
    };

    // Initialize Windy API
    windyInit(options, windyAPI => {
        // windyAPI is ready, and contain 'map', 'store',
        // 'picker' and other usefull stuff
        const {
            map
        } = windyAPI;
        // .map is instance of Leaflet map

        L.popup()
            .setLatLng([-18.918366, -48.266373])
            .setContent('Av. Rondon Pacheco ')
            .openOn(map);
    });
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript" src="jscript/graph.js"></script>
<script>
    var marcas = [
        @foreach($iot as $iots) {
            datas: "{{ $iots->data }}",
            chuva: "{{round(( ($iots->pluviometro * 0.28) - 36.4 ), 2) }}",
        },
        @endforeach
    ];
    console.log(marcas.length);
    var data = [];
    var t1 = [];
    for (i = 0; i < marcas.length; i++) {
        data.push(marcas[i].datas);
        t1.push(marcas[i].chuva);
    }
    // chart colors
    var colors = ['#37a193', '#ffbb00', '#007bff', '#000057', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d', '#66ff89', '#7d66ff', '#ff6666', '#ffff5c', '#ff5ce9', '#faa62f', '#21ff2c'];

    /* large line chart */
    var chLine = document.getElementById("chLine");

    var chartData = {
        labels: data, //["S", "M", "T", "W", "T", "F", "S"],
        datasets: [

            {
                label: 'Chuvas',
                data: t1, // [589, 445, 483, 503, 689, 692, 634],
                backgroundColor: colors[0],
                borderColor: colors[0],
                borderWidth: 1,
                pointBorderWidth: 1,
                pointBackgroundColor: colors[0],
            },

        ]
    };


    if (chLine) {
        new Chart(chLine, {
            type: 'bar',
            data: chartData,
            options: {

                scales: {
                    yAxes: [{
                        ticks: {
                            reverse: false,
                            beginAtZero: false
                        }
                    }]
                },
                legend: {
                    fontSize: 40,
                    display: true,

                }
            }
        });
    }
    //}
</script>

<script>
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpg',
        jpeg_quality: 90,
        //swfURL: 'http://192.168.0.12/cameraip',
    });

    Webcam.attach('#my_camera');
    var teste;

    function take_snapshot() {
        Webcam.snap(function(data_uri) {

            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img id="base64image" src="' + data_uri + '"/>';
        });


        var file = dataURLtoFile(document.getElementById("base64image").src, "teste.jpg")
        console.log(file);

        const formData = new FormData()
        formData.append('file', new Blob([file]));
        fetch('http://178.128.156.247/api/upload', {
                method: 'POST',
                body: formData
            })
            .then(r => r.json())
            .then(res => {
                var compara = res['resultado'].displayName;
                document.getElementById('resultado').innerHTML = res['resultado'].displayName;
                console.log(res);
                console.log(compara) ; 
               
                if(compara == 'Chuva_grau_03')
              {
                console.log('MANDAR o SMS');
              }
            })

    }


    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], filename, {
            type: mime
        });
    }


    setInterval(function() {
        take_snapshot();
    }, 20000);
</script>

@stop