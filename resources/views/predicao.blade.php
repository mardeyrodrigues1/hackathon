@extends('adminlte::page')
@section('title', 'Predição')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<div class="container">
    <h1 class="text-center">Camera 01 </h1>

    <form method="POST" action="">
        <div class="row">
            <div class="col-md-6">
                <div id="my_camera"></div>
                <br />

                <div id="resultado"> </div>
                <input type=button value="Take Snapshot" onClick="take_snapshot()">
                <input type="hidden" name="image" class="image-tag">
            </div>
            <div class="col-md-6">

               <div id="results"></div> 

            </div>
            <div class="col-md-12 text-center">
                <br />

            </div>
        </div>
    </form>
</div>



@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<!-- Configure a few settings and attach camera -->
<script>
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpg',
        jpeg_quality: 90,
        //swfURL: 'http://192.168.0.12/cameraip',
    });

    Webcam.attach('#my_camera');
    var teste;

    function take_snapshot() {
        Webcam.snap(function(data_uri) {

            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img id="base64image" src="' + data_uri + '"/>';
        });


        var file = dataURLtoFile(document.getElementById("base64image").src, "teste.jpg")
        console.log(file);

        const formData = new FormData()
        formData.append('file', new Blob([file]));
        fetch('http://178.128.156.247/api/upload', {
                method: 'POST',
                body: formData
            })
            .then(r => r.json())
            .then(res => {

                var compara = res['resultado'].displayName;
                document.getElementById('resultado').innerHTML =  res['resultado'].displayName ;
            
            console.log(compara) ; 
                if(compara == 'Chuva_grau_03')
              {
                console.log('MANDAR o SMS');
              }
                console.log(res);
            })
    }


    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], filename, {
            type: mime
        });
    }


    setInterval(function() {
        take_snapshot();
    }, 20000);
</script>
@stop